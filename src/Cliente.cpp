#include "Cliente.hpp"
#include <vector>
#include <iostream>
#include <fstream>

Cliente::Cliente()
{
    this->nome = "";
    this->idade = '0';
    this->cpf = "";
    this->email = "";
}

Cliente::Cliente(std::string nome, std::string cpf, std::string email, std::string idade, bool socio)
{
    this->nome = nome;
    this->idade = idade;
    this->cpf = cpf;
    this->email = email;
    this->socio = socio;
}

Cliente::~Cliente()
{

}

void Cliente::set_nome(std::string nome)
{
    this->nome = nome;
}

std::string Cliente::get_nome()
{
    return this->nome;
}

void Cliente::set_idade(std::string idade)
{
    this->idade = idade;
}

std::string Cliente::get_idade()
{
    return this->idade;
}

void Cliente::set_cpf(std::string cpf)
{
    this->cpf = cpf;
}

std::string Cliente::get_cpf()
{
    return this->cpf;
}

void Cliente::set_email(std::string email)
{
    this->email = email;
}

std::string Cliente::get_email()
{
    return this->email;
}

void Cliente::set_socio(bool socio)
{
    this->socio = socio;
}

bool Cliente::get_socio()
{
    return this->socio;
}

/*
    A função imprime os dados do cliente na tela após o
    cadastro para confirmação dos dados passados.
*/
void Cliente::imprime_dados(std::vector <Cliente *> clientes)
{
    std::cout << "Nome: " << clientes[clientes.size()-1]->get_nome() << std::endl;
    std::cout << "CPF: " << clientes[clientes.size()-1]->get_cpf() << std::endl;
    std::cout << "Email: " << clientes[clientes.size()-1]->get_email() << std::endl;
    std::cout << "Idade: " << clientes[clientes.size()-1]->get_idade() << std::endl;
}

// A função cadastra o Cliente, inserindo suas informações no vector clientes
std::vector <Cliente *> Cliente::cadastro(std::vector <Cliente *> clientes, std::string nome, std::string cpf, std::string idade, std::string email)
{
    this->nome = nome;
    this->idade = idade;
    this->cpf = cpf;
    this->email = email;
    this->socio = false;
    clientes.push_back(new Cliente(nome, cpf, email, idade, false));
    return clientes;
}