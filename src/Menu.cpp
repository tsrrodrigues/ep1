#include "Menu.hpp"

#include "Carrinho.hpp"
#include "functions.hpp"
#include <iostream>
#include <fstream>
#include <iomanip>

Menu::Menu()
{
    this->modo = "";
    this->clientes = lista_clientes(clientes);
    this->produtos = lista_produtos(produtos);
    this->recomendacoes = lista_recomendacoes(recomendacoes);
}

Menu::~Menu()
{
    atualiza_clientes(clientes);
    atualiza_produtos(produtos);
    atualiza_recomendacoes(recomendacoes);
}

std::string Menu::get_modo()
{
    return this->modo;
}

void Menu::set_modo(std::string modo)
{
    this->modo = modo;
}

/*
    Esta função é chamada ao iniciar o programa ou ao sairmos de alguns dos 3 modos
    (Venda, Estoque ou Recomendação). Ela mostra o menu inicial e lê qual modo o
    usuário deseja acessar.
*/
void Menu::menu_inicial()
{
    atualiza_clientes(clientes);
    atualiza_produtos(produtos);
    atualiza_recomendacoes(recomendacoes);
    
    std::cout << "---" << std::endl;
    std::cout << "MENU INICIAL" << std::endl;
    std::cout << "---" << std::endl;
    std::cout << "Digite o número correspondente ao modo de operação desejado" << std::endl;
    std::cout << "1) Modo Venda" << std::endl;
    std::cout << "2) Modo Estoque" << std::endl;
    std::cout << "3) Modo Recomendação" << std::endl;
    std::cout << "4) Sair do programa" << std::endl;

    std::string modo;
    do {
        getline(std::cin, modo);
        if (!valida_entrada(modo, '1', '4'))
            std::cout << "Entrada Inválida, digite novamente: " << std::endl;
    } while (!valida_entrada(modo, '1', '4'));

    this->set_modo(modo);
    system("clear");
}

/*
    Esta função reprenseta o Modo Venda e é chamada quando o usuário deseja acessar o mesmo.
    Ela implementa todo o Modo Venda, realizando uma nova venda caso a loja possua produtos
    suficientes para isto.
*/
void Menu::menu_venda()
{
    std::cout << "---" << std::endl;
    std::cout << "MODO VENDA - NOVA VENDA" << std::endl;
    std::cout << "---" << std::endl;
    Cliente cliente;
    std::string cpf, nome, email, idade;

    std::cout << "Insira o CPF do Cliente: ";
    do {
        getline(std::cin, cpf);
        if (!valida_cpf(cpf))
            std::cout << "CPF Inválido, digite novamente: " << std::endl;
    } while (!valida_cpf(cpf));

    std::cout << "---" << std::endl;
    if (cliente_existe(cpf, clientes))
    {
        std::cout << "O cliente já é sócio da loja e recebe desconto de 15% no valor total da compra" << std::endl;
        cliente.set_cpf(cpf);
        cliente.set_socio(true);
    }
    else if(!cliente_existe(cpf, clientes))
    {
        std::cout << "Cliente não cadastrado, para fazer o cadastro insira os seguintes dados" << std::endl; 
        
        std::cout << "Nome: ";
        std::getline(std::cin, nome);
        
        std::cout << "Idade: ";
        do {
            getline(std::cin, idade);
            if (idade[0] < '1' or idade[0] > '9')
                std::cout << "Entrada Inválida, digite novamente: " << std::endl;
        } while (idade[0] < '1' or idade[0] > '9');
        
        std::cout << "E-mail: ";
        do {
            std::cin >> email;
            if (!valida_email(email))
                std::cout << "Email Inválido, digite novamente: " << std::endl; 
        } while (!valida_email(email));
        std::cout << std::endl;
        getchar();

        clientes = cliente.cadastro(clientes, nome, cpf, idade, email);
        cliente.imprime_dados(clientes);
    }

    Carrinho carrinho;
    std::map <std::string, int> lista_produtos;

    std::cout << "---" << std::endl;
    std::cout << "Digite 1 para adicionar um produto ao carrinho" << std::endl;
    std::string acao;
    do {
        getline(std::cin, acao);
        if (!valida_entrada(acao, '0', '1'))
            std::cout << "Entrada Inválida, digite novamente: " << std::endl;
    } while (!valida_entrada(acao, '0', '1'));
    
    while(acao[0] == '1')
    {
        std::string codigo;
        std::cout << "Digite o código do produto: ";
        std::cin >> codigo;
        int quantidade;
        std::cout << "Digite a quantidade deste produto que queira adicionar ao carrinho: ";
        std::cin >> quantidade;
        getchar();

        carrinho.insere_produto_na_lista(codigo, quantidade, produtos);
        

        std::cout << std::endl << "Digite 1 para adicionar um produto ao carrinho" << std::endl;
        std::cout << "Digite 0 se deseja finalizar a compra" << std::endl;
        do {
            getline(std::cin, acao);
            if (!valida_entrada(acao, '0', '1'))
                std::cout << "Entrada Inválida, digite novamente: " << std::endl;
        } while (!valida_entrada(acao, '0', '1'));
    }

    if (carrinho.verifica_compra(produtos) and carrinho.get_lista_produtos().size() > 0)
    {
        std::cout << "---" << std::endl;
        carrinho.imprime_lista_produtos(produtos);
        std::cout << "---" << std::endl;
        carrinho.calcula_valor_total(cliente.get_socio());

        std::cout << "---" << std::endl;
        std::cout << std::endl << "Selecione a forma de pagamento" << std::endl;
        std::cout << "1) Cartão" << std::endl;
        std::cout << "2) Dinheiro" << std::endl;

        std::string pagamento;
        do {
            getline(std::cin, pagamento);
            if (!valida_entrada(pagamento, '1', '2'))
                std::cout << "Entrada Inválida, digite novamente: " << std::endl;
        } while (!valida_entrada(pagamento, '1', '2'));

        if (pagamento[0] == '2')
        {
            double valor_pago;
            std::cout << "Digite o valor dado pelo cliente: R$";
            std::cin >> valor_pago;
            getchar();

            std::cout << std::fixed << std::setprecision(2);
            std::cout << "Troco: R$";
            std::cout << carrinho.calcula_troco(valor_pago) << std::endl;

        }
        std::cout << "---" << std::endl;
        std::cout << "Compra Finalizada!" << std::endl;
        std::cout << "---" << std::endl;
        std::cout << "Você será redirecionado ao Menu Principal" << std::endl;
        std::cout << "---" << std::endl << std::endl;
        carrinho.atualiza_estoque(produtos);
        Recomendacao recomendacao;
        recomendacoes = recomendacao.atualiza_cliente_recomendacoes(cliente.get_cpf(), recomendacoes, carrinho.get_lista_produtos());

    }
    else if (!carrinho.verifica_compra(produtos))
    {
        std::cout << "---" << std::endl;
        std::cout << "Compra cancelada, não há itens no estoque suficientes para finalizar a compra." << std::endl;
    }
    else if (carrinho.get_lista_produtos().size() == 0)
    {
        std::cout << "---" << std::endl;
        std::cout << "Compra cancelada, não há itens no carrinho suficientes para realizar a compra." << std::endl;
    }
}

/*
    Esta função reprenseta o Modo Estoque e é chamada quando o usuário deseja acessar o mesmo.
    Ela implementa todo o Modo Estoque, podendo adicionar produtos novos ou já existentes
    no estoque.
*/
void Menu::menu_estoque()
{
    std::cout << "---" << std::endl;
    std::cout << "MODO ESTOQUE" << std::endl;
    std::cout << "---" << std::endl;
    std::cout << std::endl << "Digite 1 para adicionar um produto ao estoque" << std::endl;
    std::cout << "Digite 0 para sair do MODO ESTOQUE" << std::endl;
    std::string acao;
    do {
        getline(std::cin, acao);
        if (!valida_entrada(acao, '0', '1'))
            std::cout << "Entrada Inválida, digite novamente: " << std::endl;
    } while (!valida_entrada(acao, '0', '1'));

    Produto produto;
    while (acao[0] == '1')
    {
        int quantidade;
        double valor;
        std::string nome, categoria, codigo;

        std::cout << "Digite o código do produto: ";
        std::cin >> codigo;
        
        if (produto.produto_existe(produtos, codigo))
        {
            std::cout << "O produto já está cadastrado no estoque" << std::endl;
            std::cout << "Digite a quantidade deste produto que queira adicionar ao estoque" << std::endl;
            std::cin >> quantidade;
            getchar();
            produtos = produto.adiciona_produto(produtos, quantidade, codigo);
        }
        else if (!produto.produto_existe(produtos, codigo))
        {
            std::cout << "O produto não está cadastrado no estoque, para cadastrar digite os seguintes dados:" << std::endl;
            std::cout << "Nome: ";
            getchar();
            getline(std::cin, nome);
            std::cout << std::endl;
            std::cout << "Digite abaixo a(s) categoria(s) do produto" << std::endl;
            std::cout << "Caso o produto possua mais que uma categoria digite-as separando por espaços" << std::endl;
            std::cout << "Caso uma das categorias possuir nome composto utilize o caractere -(hífen) para separar as palavras" << std::endl;
            std::cout << "Ex.: alimento-perecível" << std::endl;
            getline(std::cin, categoria);
            std::cout << "Valor Unitário: ";
            std::cin >> valor;
            std::cout << "Quantidade: ";
            std::cin >> quantidade;
            getchar();
            std::cout << std::endl;

            produtos = produto.cadastro(produtos, nome, categoria, valor, codigo, quantidade);
        }
        
        std::cout << "Digite 1 se quer adicionar mais um produto ao estoque" << std::endl;
        std::cout << "Digite 0 se deseja sair do modo estoque" << std::endl;
        do {
            getline(std::cin, acao);
            if (!valida_entrada(acao, '0', '1'))
                std::cout << "Entrada Inválida, digite novamente: " << std::endl;
        } while (!valida_entrada(acao, '0', '1'));
    }
    produto.lista_estoque(produtos);
    std::cout << "---" << std::endl;
    std::cout << "Você será redirecionado ao Menu Principal" << std::endl;
    std::cout << "---" << std::endl << std::endl;
}

/*
    Esta função reprenseta o Modo Recomendação e é chamada quando o usuário deseja acessar o mesmo.
    Ela implementa todo o Modo Recomendação, imprimindo na tela os produtos mais comprados pelo
    cliente buscado.
*/
void Menu::menu_recomendacao()
{
    std::cout << "---" << std::endl;
    std::cout << "MODO RECOMENDAÇÃO" << std::endl;
    std::cout << "---" << std::endl;
    std::string acao = "1";
    while (acao[0] == '1')
    {
        std::string cpf;
        std::cout << std::endl << "Para procurar recomendações, digite o CPF do cliente: ";
        do {
            getline(std::cin, cpf);
            if (!valida_cpf(cpf))
                std::cout << "CPF Inválido, digite novamente: " << std::endl;
        } while (!valida_cpf(cpf));
        
        if (cliente_existe(cpf, clientes))
        {
            Recomendacao recomendacao(cpf);
            bool cliente_comprou = false;
            for (auto cliente : recomendacoes)
                if (cpf == cliente->get_cpf())
                {
                    recomendacao.lista_recomendacao(cliente->get_categorias(), cliente->get_quantidades());
                    cliente_comprou = true;
                }
            if (!cliente_comprou)
            {
                std::cout << "---" << std::endl;
                std::cout << "O cliente ainda não realizou compras nessa loja";
            }
        }

        else if (!cliente_existe(cpf, clientes))
        {
            std::cout << std::endl << "---" << std::endl;
            std::cout << "O Cliente não está cadastrado na loja" << std::endl;
            std::cout << "Não é possível continuar com a operação" << std::endl;
        }
        std::cout << std::endl << "---" << std::endl;
        std::cout << "Para procurar recomendações de um outro cliente, digite 1" << std::endl;
        std::cout << "Para voltar ao menu inicial, digite 0" << std::endl;
        do {
            getline(std::cin, acao);
            if (!valida_entrada(acao, '0', '1'))
                std::cout << "Entrada Inválida, digite novamente: " << std::endl;
        } while (!valida_entrada(acao, '0', '1'));
    }
}