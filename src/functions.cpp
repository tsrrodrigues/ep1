#include "functions.hpp"
#include "Cliente.hpp"
#include "Produto.hpp"
#include "Recomendacao.hpp"

#include <iostream>
#include <vector>
#include <fstream>

// A função verifica se um cliente ja esta cadastrado no banco de dados, percorrendo o vector clientes
bool cliente_existe(std::string cpf, std::vector <Cliente *> clientes)
{
    for (auto i : clientes)
        if (cpf == i->get_cpf())
            return true;
    return false;
}

// A função separa um string pelos seus espaços
std::vector <std::string> separa_string(std::string str, std::vector <std::string> v)
{
    std::string aux = "";
    for (unsigned int i = 0; i < str.length(); i++)
    {
        if (str[i] == ' ')
        {
            v.push_back(aux);
            aux = "";
        }
        else if (i == str.length()-1)
        {
            aux += str[i];
            v.push_back(aux);
            aux = "";
        }
        else
            aux += str[i];
    }
    return v;
}

// A função verifica se a entrada está dentro dos parâmetros passados
bool valida_entrada(std::string entrada, char parametro1, char parametro2)
{
    if (entrada[0] >= parametro1 and entrada[0] <= parametro2 and entrada[1] == '\0')
        return true;
    return false;
}

/* 
    A função verifica se o cpf passado é um cpf válido.
    CPF válido neste caso é um CPF existente de acordo com os digitos verificadores.
*/
bool valida_cpf(std::string cpf)
{
    int sum = 0, cont = 10, posicao = 0;
    for (char i : cpf)
    {
        sum += cont*(i-'0');
        --cont;
        if (posicao == 8)
            break;
        ++posicao;
    }
    sum *= 10;
    if (sum % 11 == cpf[9]-'0' or (sum % 11 == 10 and cpf[9] == '0'))
    {
        sum = 0, cont = 11, posicao = 0;
        for (char i : cpf)
        {
            sum += cont*(i-'0');
            --cont;
            if (posicao == 9)
                break;
            ++posicao;
        }
        sum *= 10;
        if (sum % 11 == cpf[10]-'0' or (sum % 11 == 10 and cpf[10] == '0'))
        {
            return true;
        }
        return false;
    }
    return false;
}

/*
    A função verifica se o email passado é um email válido.
    Email válido nesta função é aquele que possui um dos provedores abaixo
*/
bool valida_email(std::string email)
{
    for (unsigned int i = 0; i < email.size(); i++)
    {
        if (email[i] == '@')
        {
            std::string temp = email.substr(i+1);
            if (temp == "outlook.com")
                return true;
            else if (temp == "hotmail.com")
                return true;
            else if (temp == "live.com")
                return true;
            else if (temp == "gmail.com")
                return true;
            else if (temp == "bol.com.br")
                return true;
            else if (temp == "uol.com.br")
                return true;
            else if (temp == "yahoo.com")
                return true;
            else if (temp == "ymail.com")
                return true;
            else if (temp == "globomail.com")
                return true;
        }
    }
    return false;
}

/*
    Ao iniciar o programa, os clientes presentes no arquivo clientes.txt são inseridos em um vector
    através dessa função, para tratamento mais fácil. O vector é usado ao decorrer do programa.
    O vector "clientes" é inicializado ao iniciarmos o programa. Ele é inicializado no construtor
    da classe Menu.
*/
std::vector <Cliente *> lista_clientes(std::vector <Cliente *> clientes)
{
    std::string info;
    std::ifstream dados_clientes;
    dados_clientes.open("txt/clientes.txt", std::ifstream::in);

    if (dados_clientes.is_open())
    {
        int contador = 1, posicao = 0;
        while (getline(dados_clientes, info))
        {
            switch(contador)
            {
                case 1:
                    clientes.push_back(new Cliente());
                    clientes[posicao]->set_cpf(info);
                    break;
                case 2:
                    clientes[posicao]->set_nome(info);
                    break;
                case 3:
                    clientes[posicao]->set_idade(info);
                    break;
                case 4:
                    clientes[posicao]->set_email(info);
                    break;
                default:
                    contador = 0;
                    posicao++;
                    break;
            }
            ++contador;
        }
    }
    return clientes;
}

/*
    Ao iniciar o programa, os produtos presentes no arquivo produtos.txt são inseridos em um vector
    através dessa função, para tratamento mais fácil. O vector é usado ao decorrer do programa.
    O vector "produtos" é inicializado ao iniciarmos o programa. Ele é inicializado no construtor
    da classe Menu.
*/
std::vector <Produto *> lista_produtos(std::vector <Produto *> produtos)
{
    std::string info;
    std::ifstream dados_produtos;
    dados_produtos.open("txt/produtos.txt", std::ifstream::in);

    if (dados_produtos.is_open())
    {
        int contador = 1, posicao = 0;
        while (getline(dados_produtos, info))
        {
            switch(contador)
            {
                case 1:
                    produtos.push_back(new Produto());
                    produtos[posicao]->set_codigo(info);
                    break;
                case 2:
                    produtos[posicao]->set_nome(info);
                    break;
                case 3:
                    produtos[posicao]->set_valor(stod(info));
                    break;
                case 4:
                    info.erase(info.find_last_not_of(" ")+1);
                    produtos[posicao]->set_categoria(info);
                    break;
                case 5:
                    produtos[posicao]->set_quantidade(stoi(info));
                    break;
                default:
                    contador = 0;
                    posicao++;
                    break;
            }
            ++contador;
        }
    }
    return produtos;
}

/*
    Ao iniciar o programa, as informações presentes no arquivo recomendacoes.txt são inseridos em
    um vector através dessa função, para tratamento mais fácil. O vector é usado ao decorrer do programa.
    O vector "recomendacoes" é inicializado ao iniciarmos o programa. Ele é inicializado no construtor
    da classe Menu.
*/
std::vector <Recomendacao *> lista_recomendacoes(std::vector <Recomendacao *> recomendacoes)
{
    std::string info;
    std::ifstream dados_recomendacoes;
    dados_recomendacoes.open("txt/recomendacoes.txt", std::ifstream::in);

    if (dados_recomendacoes.is_open())
    {
        int contador = 1, posicao = 0;
        std::vector <std::string> aux_quantidades, aux_categorias;
        while (getline(dados_recomendacoes, info))
        {
            switch(contador)
            {
                case 1:
                    recomendacoes.push_back(new Recomendacao());
                    recomendacoes[posicao]->set_cpf(info);
                    break;
                case 2:
                    aux_categorias = separa_string(info, aux_categorias);
                    for (auto i : aux_categorias)
                        recomendacoes[posicao]->categorias.push_back(i);
                    break;
                case 3:
                    aux_quantidades = separa_string(info, aux_quantidades);
                    for (auto i : aux_quantidades)
                        recomendacoes[posicao]->quantidades.push_back(stoi(i));
                    break;
                default:
                    aux_categorias.clear();
                    aux_quantidades.clear();
                    contador = 0;
                    posicao++;
                    break;
            }
            ++contador;
        }
    }
    return recomendacoes;
}

/*
    Ao finalizar o programa, os clientes presentes no vector clientes são inseridos no arquivo
    clientes.txt através dessa função. A função é chamada no destrutor da classe Menu.
*/
void atualiza_clientes(std::vector <Cliente *> clientes)
{
    std::ofstream saida;
    saida.open("txt/clientes.txt");
    if(saida.is_open())
    {
        for (auto cliente : clientes)
        {
            saida << cliente->get_cpf() << std::endl;
            saida << cliente->get_nome() << std::endl;
            saida << cliente->get_idade() << std::endl;
            saida << cliente->get_email() << std::endl;
            saida << std::endl;
        }
    }
}

/*
    Ao finalizar o programa, os produtos presentes no vector produtos são inseridos no arquivo
    produtos.txt através dessa função. A função é chamada no destrutor da classe Menu.
*/
void atualiza_produtos(std::vector <Produto *> produtos)
{
    std::ofstream saida;
    saida.open("txt/produtos.txt");
    if(saida.is_open())
    {
        for (auto produto : produtos)
        {
            saida << produto->get_codigo() << std::endl;
            saida << produto->get_nome() << std::endl;
            saida << produto->get_valor() << std::endl;
            saida << produto->get_categoria().erase(produto->get_categoria().find_last_not_of(" ")+1) << std::endl;
            saida << produto->get_quantidade() << std::endl;
            saida << std::endl;
        }
    }
}

/*
    Ao finalizar o programa, as recomendações presentes no vector recomendacoes são inseridos
    no arquivo recomendacoes.txt através dessa função. A função é chamada no destrutor da classe Menu.
*/
void atualiza_recomendacoes(std::vector <Recomendacao *> recomendacoes)
{
    if (recomendacoes.size() > 0)
    {
        std::ofstream saida;
        saida.open("txt/recomendacoes.txt");
        if(saida.is_open())
        {
            for (auto info : recomendacoes)
            {
                saida << info->get_cpf() << std::endl;
                for (auto i : info->get_categorias())
                    saida << i.erase(i.find_last_not_of(" ")+1) << " ";
                saida << std::endl;
                for (auto i : info->get_quantidades())
                    saida << i << " ";
                saida << std::endl;
                saida << std::endl;
            }
        }
    }
}