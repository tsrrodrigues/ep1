#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <utility>
#include <iomanip>

#include "Menu.hpp"
#include "functions.hpp"
#include "Cliente.hpp"
#include "Produto.hpp"
#include "Carrinho.hpp"

int main ()
{
    Menu menu;
    do {
        menu.menu_inicial();

        // MODO VENDA
        if (menu.get_modo()[0] == '1')
            menu.menu_venda();

        // MODO ESTOQUE
        else if (menu.get_modo()[0] == '2')
            menu.menu_estoque();

        else if (menu.get_modo()[0] == '3')
            menu.menu_recomendacao();
    } while (valida_entrada(menu.get_modo(), '1', '3'));

    return 0;
}