#include "Produto.hpp"
#include <stdarg.h>
#include <fstream>
#include <vector>
#include <iostream>

Produto::Produto()
{
    this->nome = "";
    this->codigo = "0000";
    this->quantidade = 0;
    this->categoria = "";
    this->valor = 0.0;
}

Produto::Produto(std::string nome, std::string codigo, int quantidade, std::string categoria, double valor)
{
    this->nome = nome;
    this->codigo = codigo;
    this->quantidade = quantidade;
    this->categoria = categoria;
    this->valor = valor;
}

Produto::~Produto()
{

}

void Produto::set_nome(std::string nome)
{
    this->nome = nome;
}

std::string Produto::get_nome()
{
    return this->nome;
}

void Produto::set_codigo(std::string codigo)
{
    this->codigo = codigo;
}

std::string Produto::get_codigo()
{
    return this->codigo;
}

void Produto::set_quantidade(int quantidade)
{
    this->quantidade += quantidade;
}

int Produto::get_quantidade()
{
    return this->quantidade;
}

void Produto::set_categoria(std::string categoria)
{
    this->categoria.append(categoria);
    this->categoria.append(" ");
}

std::string Produto::get_categoria()
{
    return this->categoria;
}

void Produto::set_valor(double valor)
{
    this->valor = valor;
}

double Produto::get_valor()
{
    return this->valor;
}


// Verifica se um produto ja está cadastrado no estoque percorrendo o vector produtos
bool Produto::produto_existe(std::vector <Produto *> produtos, std::string codigo)
{
    for (auto i : produtos)
        if (codigo == i->get_codigo())
            return true;
    return false;
}

// Adiciona uma quantidade a um produto ja existente no estoque
std::vector <Produto *> Produto::adiciona_produto(std::vector <Produto *> produtos, int quantidade, std::string codigo)
{
    for (auto i : produtos)
        if (i->get_codigo() == codigo)
            i->set_quantidade(quantidade);
    return produtos;
}

// Cadastra um produto no estoque
std::vector <Produto *> Produto::cadastro(std::vector <Produto *> produtos, std::string nome, std::string categoria, double valor, std::string codigo, int quantidade)
{
    produtos.push_back(new Produto(nome, codigo, quantidade, categoria, valor));
    return produtos;
}

// Imprime os produtos cadastrados no estoque
void Produto::lista_estoque(std::vector <Produto *> produtos)
{
    std::cout << std::endl << "---" << std::endl;
    std::cout << std::endl << "PRODUTOS NO ESTOQUE:" << std::endl << std::endl;
    for(auto i : produtos)
    {
        std::cout << "Codigo: " << i->get_codigo() << std::endl;
        std::cout << "Nome: " << i->get_nome() << std::endl;
        std::cout << "Valor: " << i->get_valor() << std::endl;
        std::cout << "Categoria(s): " << i->get_categoria() << std::endl;
        std::cout << "Quantidade: " << i->get_quantidade() << std::endl;
        std::cout << std::endl;
    }
}