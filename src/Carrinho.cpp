#include "Carrinho.hpp"
#include <fstream>
#include <map>
#include <iomanip>

Carrinho::Carrinho()
{
    cpf_cliente = "";
    preco_total = 0.0;
}

Carrinho::~Carrinho()
{

}

void Carrinho::set_cpf_cliente(std::string cpf_cliente)
{
    this->cpf_cliente = cpf_cliente;
}

std::string Carrinho::get_cpf_cliente()
{
    return this->cpf_cliente;
}

void Carrinho::set_preco_total(double preco_total)
{
    this->preco_total = preco_total;
}

double Carrinho::get_preco_total()
{
    return this->preco_total;
}

std::map <std::string, std::pair<int, std::string>> Carrinho::get_lista_produtos()
{
    return this->lista_produtos;
}

// A função insere um produto no map que possui os produtos do carrinho
void Carrinho::insere_produto_na_lista(std::string codigo, int quantidade, std::vector <Produto *> produtos)
{
    std::string categorias;
    for (auto prod : produtos)
        if (prod->get_codigo() == codigo)
            categorias = prod->get_categoria();
    this->lista_produtos.insert({codigo, std::make_pair(quantidade, categorias)});
}

// A função imprime os produtos passados no caixa
void Carrinho::imprime_lista_produtos(std::vector <Produto *> produtos)
{
    std::cout << std::endl;
    std::cout << "PRODUTOS NO CARRINHO:" << std::endl;
    for (auto produto : this->lista_produtos)
    {
        for (auto produto_no_estoque : produtos)
            if (produto.first == produto_no_estoque->get_codigo())
            {
                std::cout << "Nome: " << produto_no_estoque->get_nome() << std::endl;
                this->set_preco_total(produto_no_estoque->get_valor()*(double)produto.second.first + this->get_preco_total());
            }
        std::cout << "Quantidade: " << produto.second.first << std::endl;
        std::cout << std::endl;
    }
}

/*
    A função verifica se os produtos passados no caixa possuem
    quantidade no estoque que favoreça a realização da compra
*/
bool Carrinho::verifica_compra(std::vector <Produto *> produtos)
{
    unsigned int produtos_verificados = 0;
    for (auto produto : this->lista_produtos)
        for (auto prod_no_estoque : produtos)
            if (produto.first == prod_no_estoque->get_codigo())
                if (produto.second.first <= prod_no_estoque->get_quantidade())
                    produtos_verificados++;

    if (produtos_verificados == this->lista_produtos.size())
        return true;
    return false;
}

// A função atualiza a quantidade dos produtos no estoque
void Carrinho::atualiza_estoque(std::vector <Produto *> produtos)
{
    for (auto i : this->lista_produtos)
        for (auto j : produtos)
            if (i.first == j->get_codigo())
                j->set_quantidade(-i.second.first);
}

void Carrinho::calcula_valor_total(bool socio)
{
    if (socio)
    {
        std::cout << "O cliente sócio recebeu um desconto de 15% na compra" << std::endl;
        this->set_preco_total(this->get_preco_total() * 0.85);
    }
    std::cout << std::fixed << std::setprecision(2);
    std::cout << "PREÇO TOTAL = R$" << this->get_preco_total() << std::endl;
}

double Carrinho::calcula_troco(double valor_pago)
{
    return (valor_pago - this->get_preco_total());
}