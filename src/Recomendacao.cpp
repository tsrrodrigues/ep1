#include "Recomendacao.hpp"

#include "functions.hpp"
#include "Cliente.hpp"

#include <algorithm>
#include <iostream>
#include <climits>

Recomendacao::Recomendacao()
{

}

Recomendacao::Recomendacao(std::string cpf)
{
    this->cpf = cpf;
}

Recomendacao::~Recomendacao()
{

}


std::string Recomendacao::get_cpf()
{
    return this->cpf;
}

void Recomendacao::set_cpf(std::string cpf)
{
    this->cpf = cpf;
}

std::vector <int> Recomendacao::get_quantidades()
{
    return this->quantidades;
}

void Recomendacao::set_quantidades(int quantidade)
{
    this->quantidades.push_back(quantidade);
}

std::vector <std::string> Recomendacao::get_categorias()
{
    return this->categorias;
}

void Recomendacao::set_categoria(std::string categoria)
{
    categorias.push_back(categoria);
}

/*
    Essa função é chamada ao fim de alguma venda, ela atualiza as categorias mais compradas
    pelo cliente que realizou a compra.
*/
std::vector <Recomendacao *> Recomendacao::atualiza_cliente_recomendacoes(std::string cpf, std::vector <Recomendacao *> recomendacoes,
                                std::map <std::string, std::pair<int, std::string>> lista_produtos)
{
    std::vector <int> qtd;
    std::vector <std::string> cats;
    int pos = 0, client_exists = 0;
    for (auto cliente : recomendacoes)
    {
        if (cliente->get_cpf() == cpf)
        {
            cats = cliente->get_categorias();
            qtd = cliente->get_quantidades();
            client_exists = 1;
            break;
        }
        pos++;
    }

    if (!client_exists)
        recomendacoes.push_back(new Recomendacao(cpf));

    for (auto produto : lista_produtos)
    {
        std::vector <std::string> aux_categorias;
        aux_categorias = separa_string(produto.second.second, aux_categorias);
        for (auto prod_categoria : aux_categorias)
        {
            if (std::count(cats.begin(), cats.end(), prod_categoria) == 0)
            {
                recomendacoes[pos]->set_categoria(prod_categoria);
                recomendacoes[pos]->set_quantidades(0);
                cats.push_back(prod_categoria);
                qtd.push_back(produto.second.first);
            }
            int pos2 = 0;
            for (auto cat : cats)
            {
                cat.erase(cat.find_last_not_of(" ")+1);
                prod_categoria.erase(prod_categoria.find_last_not_of(" ")+1);
                if (cat == prod_categoria)
                    recomendacoes[pos]->quantidades[pos2] +=produto.second.first;
                pos2++;
            }
        }
    }
    return recomendacoes;
}

// A função lista as categorias mais compradas pelo cliente
void Recomendacao::lista_recomendacao(std::vector <std::string> categorias, std::vector <int> quantidades)
{
    std::cout << std::endl << "---" << std::endl;
    std::cout << "As categorias mais recomendadas para o cliente são:" << std::endl << std::endl;
    for (unsigned int i = 0; i < quantidades.size(); i++)
    {
        int maior = INT_MIN, pos = 0;
        for (unsigned int j = i; j < quantidades.size(); j++)
        {
            if (quantidades[j] > maior)
            {
                maior = quantidades[j];
                pos = j;
            }
        }
        int aux = quantidades[i];
        quantidades[i] = maior;
        quantidades[pos] = aux;

        std::string aux2 = categorias[i];
        categorias[i] = categorias[pos];
        categorias[pos] = aux2;
    }

    for (unsigned int i = 0; i < quantidades.size() and i < 10; i++)
        std::cout << i+1 << ") " << categorias[i] << ": " << quantidades[i] << std::endl;
}
