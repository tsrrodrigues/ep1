#ifndef RECOMENDACAO_HPP
#define RECOMENDACAO_HPP

#include "Produto.hpp"

#include <string>
#include <map>
#include <vector>

class Recomendacao
{
    private:
        std::string cpf;
    
    public:
        std::vector <std::string> categorias;
        std::vector <int> quantidades;
        
        Recomendacao();
        Recomendacao(std::string cpf);
        ~Recomendacao();

        std::string get_cpf();
        void set_cpf(std::string cpf);

        std::vector <int> get_quantidades();
        void set_quantidades(int quantidade);

        std::vector <std::string> get_categorias();
        void set_categoria(std::string categoria);

        std::vector <Recomendacao *> atualiza_cliente_recomendacoes(std::string cpf,
                                    std::vector <Recomendacao *> recomendacoes,
                                    std::map <std::string, std::pair<int, std::string>> lista_produtos);

        void lista_recomendacao(std::vector <std::string> categorias, std::vector <int> quantidades);
};

#endif