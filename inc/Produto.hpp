#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <string>
#include <vector>

class Produto
{
    private:
        std::string nome;
        std::string codigo;
        int quantidade;
        std::string categoria;
        double valor;

    public:
        Produto();
        Produto(std::string nome, std::string codigo, int quantidade, std::string categoria, double valor);
        ~Produto();

        void set_nome(std::string nome);
        std::string get_nome();

        void set_codigo(std::string codigo);
        std::string get_codigo();

        void set_quantidade(int quantidade);
        int get_quantidade();

        void set_categoria(std::string categoria);
        std::string get_categoria();

        void set_valor(double valor);
        double get_valor();

        bool produto_existe(std::vector <Produto *> produtos, std::string codigo);

        std::vector <Produto *> adiciona_produto(std::vector <Produto *> produtos, int quantidade, std::string codigo);

        std::vector <Produto *> cadastro(std::vector <Produto *> produtos, std::string nome, std::string categoria, double valor, std::string codigo, int quantidade);

        void lista_estoque(std::vector <Produto *> produtos);
};

#endif