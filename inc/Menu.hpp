#ifndef MENU_HPP
#define MENU_HPP

#include "Cliente.hpp"
#include "Produto.hpp"
#include "Recomendacao.hpp"

#include <string>
#include <vector>

class Menu
{
    private:
        std::string modo;
        //O vector clientes possui as informações dos clientes cadastrados
        std::vector <Cliente *> clientes;
        //O vector produtos possui as informações dos produtos no estoque
        std::vector <Produto *> produtos;
        //O vector recomendacoes possui as informações de recomendações para os clientes
        std::vector <Recomendacao *> recomendacoes;
    public:
        Menu();
        ~Menu();

        std::string get_modo();
        void set_modo(std::string modo);
        
        void menu_inicial();
        
        void menu_venda();

        void menu_estoque();

        void menu_recomendacao();
};
#endif