#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <string>
#include <vector>

class Cliente
{
    private:
        std::string nome;
        std::string idade;
        std::string cpf;
        std::string email;
        bool socio;
    
    public:
        Cliente();
        Cliente(std::string nome, std::string cpf, std::string email, std::string idade, bool socio);
        ~Cliente();

        void set_nome(std::string nome);
        std::string get_nome();

        void set_idade(std::string idade);
        std::string get_idade();

        void set_cpf(std::string cpf);
        std::string get_cpf();

        void set_email(std::string email);
        std::string get_email();

        void set_socio(bool socio);
        bool get_socio();

        void imprime_dados(std::vector <Cliente *> clientes);

        std::vector <Cliente *> cadastro(std::vector <Cliente *> clientes,
                                                    std::string nome, std::string cpf,
                                                    std::string idade, std::string email);
};

#endif