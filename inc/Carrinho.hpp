#ifndef CARRINHO_HPP
#define CARRINHO_HPP

#include "Cliente.hpp"
#include "Produto.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <utility>

class Carrinho {
    private:
        std::string cpf_cliente;
        double preco_total;
        /*
            O map lista_produtos possui todos os produtos passados no caixa na hora
            da venda na seguinte forma: "map[codigo] = {quantidade, categoria}"
        */
        std::map <std::string, std::pair<int, std::string>> lista_produtos;
    
    public:
        Carrinho();
        ~Carrinho();

        void set_cpf_cliente(std::string cpf_cliente);
        std::string get_cpf_cliente();
        
        void set_preco_total(double preco_total);
        double get_preco_total();

       std::map <std::string, std::pair<int, std::string>> get_lista_produtos();

        void insere_produto_na_lista(std::string codigo, int quantidade, std::vector <Produto *> produtos);

        void imprime_lista_produtos(std::vector <Produto *> produtos);

        bool verifica_compra(std::vector <Produto *> produtos);

        void atualiza_estoque(std::vector <Produto *> produtos);

        void calcula_valor_total(bool socio);

        double calcula_troco(double valor_pago);
};

#endif