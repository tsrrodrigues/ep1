#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

/*
    Os arquivos functions.hpp e functions.cpp foram criados para conter funções que auxiliam
    algumas funcionalidades do programa.
*/

#include <string>
#include <vector>
#include "Cliente.hpp"
#include "Produto.hpp"
#include "Recomendacao.hpp"

bool cliente_existe(std::string cpf, std::vector <Cliente *> clientes);

std::vector <std::string> separa_string(std::string str, std::vector <std::string> v);

bool valida_entrada(std::string entrada, char parametro1, char parametro2);

bool valida_cpf(std::string cpf);

bool valida_email(std::string email);

std::vector <Cliente *> lista_clientes(std::vector <Cliente *> clientes);

std::vector <Produto *> lista_produtos(std::vector <Produto *> produtos);

std::vector <Recomendacao *> lista_recomendacoes(std::vector <Recomendacao *> recomendacoes);

void atualiza_clientes(std::vector <Cliente *> clientes);

void atualiza_produtos(std::vector <Produto *> produtos);

void atualiza_recomendacoes(std::vector <Recomendacao *> recomendacoes);

#endif