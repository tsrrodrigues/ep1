var NAVTREE =
[
  [ "Programa de Venda", "index.html", [
    [ "Documentação EP1", "index.html", null ],
    [ "EP1 - OO 2019.2 (UnB - Gama)", "md__home_tiago__documentos__un_b__o_o_ep1__r_e_a_d_m_e.html", null ],
    [ "Classes", "annotated.html", [
      [ "Lista de Componentes", "annotated.html", "annotated_dup" ],
      [ "Índice dos Componentes", "classes.html", null ]
    ] ],
    [ "Arquivos", null, [
      [ "Lista de Arquivos", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_carrinho_8hpp_source.html"
];

var SYNCONMSG = 'clique para desativar a sincronização do painel';
var SYNCOFFMSG = 'clique para ativar a sincronização do painel';