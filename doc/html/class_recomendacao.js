var class_recomendacao =
[
    [ "Recomendacao", "class_recomendacao.html#a0612ef7f06aefacab2c8a940394f2458", null ],
    [ "Recomendacao", "class_recomendacao.html#a1fb475ef0e0153198c8bf27e4759e178", null ],
    [ "~Recomendacao", "class_recomendacao.html#a8a8097ea1bf3726f4a94e56743e5658f", null ],
    [ "atualiza_cliente_recomendacoes", "class_recomendacao.html#aef5043ea076f80b68154f56054f02e98", null ],
    [ "get_categorias", "class_recomendacao.html#a75cc4a666b4e97a8972a8db9133fd5ad", null ],
    [ "get_cpf", "class_recomendacao.html#a7a7201f18b4453fd3200265e7b1d515b", null ],
    [ "get_quantidades", "class_recomendacao.html#a2882a2764304bba0e682c682d2148385", null ],
    [ "lista_recomendacao", "class_recomendacao.html#a3ca3c2230ff8c1c7eaff5eefbea513c3", null ],
    [ "set_categoria", "class_recomendacao.html#ad659b2939ebe77e3179bab07231670b9", null ],
    [ "set_cpf", "class_recomendacao.html#a29d548d9ef6cae35a8cc994a3359c149", null ],
    [ "set_quantidades", "class_recomendacao.html#a558b19ac70810d37a7fe89d020b65b87", null ],
    [ "categorias", "class_recomendacao.html#af5050542556d2ad916d7675fddf44dab", null ],
    [ "quantidades", "class_recomendacao.html#a5a39242461658a21de40dc9268c6f6e2", null ]
];