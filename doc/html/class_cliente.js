var class_cliente =
[
    [ "Cliente", "class_cliente.html#ab6a372f412692c12c4be4427b00a3f6e", null ],
    [ "Cliente", "class_cliente.html#a9456ae4d618e417ae1dfeac6862e2d98", null ],
    [ "~Cliente", "class_cliente.html#a29d1d53394350c66363109e33c990b58", null ],
    [ "cadastro", "class_cliente.html#a05e207632eeff123b6fa4af8e68bcb8f", null ],
    [ "get_cpf", "class_cliente.html#a34024580fa30f7d4cc353fef7e99bb7e", null ],
    [ "get_email", "class_cliente.html#a2858b6744b07aee4e180d8b441eb5c51", null ],
    [ "get_idade", "class_cliente.html#a49a0dd565ec7f0ce46e35d82d8ac9bb6", null ],
    [ "get_nome", "class_cliente.html#adc90064373e2284ae082a0c3b992a18e", null ],
    [ "get_socio", "class_cliente.html#ac19d3bc1957721f87c5605d33d75c020", null ],
    [ "imprime_dados", "class_cliente.html#a8cbe9e3356baf67cd418b354d667f4d5", null ],
    [ "set_cpf", "class_cliente.html#aa3d5a858d866ba32e70d08b6e4ebab38", null ],
    [ "set_email", "class_cliente.html#a7588a428be8edc0263b141049c3d3115", null ],
    [ "set_idade", "class_cliente.html#aca8581e1523f0c77ab3d54709088c8cd", null ],
    [ "set_nome", "class_cliente.html#a666148cd1cc632a13a15420c4da7e2bd", null ],
    [ "set_socio", "class_cliente.html#a13f1249779395a989ef94541ce73435b", null ]
];