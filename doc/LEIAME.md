# Documentação EP1
# Orientação a Objetos - Turma B
# Aluno: Tiago Samuel Rodrigues
# Matrícula: 18/0114689

Este programa foi feito com o objetivo de controle de estoque e vendas de uma loja fictícia. Nele é permitido:
    * Cadastrar novos produtos no estoque.
    * Realizar Vendas.
    * Obter as categorias de produtos mais compradas por cada cliente.

O programa foi feito com o paradigma de Orientação a Objetos e com o objetivo de ser o mais
intuitivo possível. Durante o programa cada ação por parte do usuário é precedida de instruções para
o mesmo realizar corretamente.

Para ser executado é necessário entrar na pasta que posssui todos os arquivos do programa e digitar o seguintes comando no terminal:
    make
Após isso para ser executado é nessário digitar o seguinte comando:
    make run


# Especificações:

#includes necessários
	* <iostream>
	* <string>
	* <vector>
	* <map>
	* <utility>
	* <iomanip>
	* <fstream>
	* <climits>
	* <algotith

# Classes
    * Menu
    * Cliente
    * Carrinho
    * Produto
    * Recomendacao

# Menu:
A classe Menu é utilizada para chamar os Modos: Venda, Estoque e Recomendação. Assim também como mostrar o Menu Inicial.
        
## Atributos:
    std::string modo;

    std::vector <Cliente *> clientes;

    std::vector <Produto *> produtos;

    std::vector <Recomendacao *> recomendacoes;

## Funções:
    Menu();

    ~Menu();

    std::string get_modo();

    void set_modo(std::string modo);
    
    void menu_inicial();
    
    void menu_venda();

    void menu_estoque();

    void menu_recomendacao();

# Cliente:
A classe Cliente é utilizada no Modo Venda. Ela serve para termos um objetos com os dados do cliente que está realizando a compra.

Um cliente é considera sócio caso ele já tenha realizado alguma compra anteriormente na loja. Caso ele seja sócio terá um desconto de 15% na compra.

# Atributos:
std::string nome;

std::string idade;

std::string cpf;

std::string email;

bool socio;

# Funções:
Cliente();

Cliente(std::string nome, std::string cpf, std::string email, std::string idade, bool socio);

~Cliente();

void set_nome(std::string nome);

std::string get_nome();

void set_idade(std::string idade);

std::string get_idade();

void set_cpf(std::string cpf);

std::string get_cpf();

void set_email(std::string email);

std::string get_email();

void set_socio(bool socio);

bool get_socio();

void imprime_dados(std::vector <Cliente *> clientes);

std::vector <Cliente *> cadastro(std::vector <Cliente *> clientes,
                                    std::string nome, std::string cpf,
                                    std::string idade, std::string email);


# Carrinho:
A classe Carrinho é utilizada no Modo Venda para obter os produtos que o cliente deseja comprar. Cada produto é tratado com seus atributos código e categorias, assim também como a quantidade que o cliente deseja comprar daquele produto.

Quando a compra é efetuada as categorias mais compradas pelo cliente são atualizadas relacionando a compra ao seu cpf assim como a quantidade de cada produto no estoque é alterada.

## Atributos:
std::string cpf_cliente;

double preco_total;

std::map <std::string, std::pair<int, std::string>> lista_produtos;

## Funções:
Carrinho();

~Carrinho();

void set_cpf_cliente(std::string cpf_cliente);

std::string get_cpf_cliente();

void set_preco_total(double preco_total);

double get_preco_total();

std::map <std::string, std::pair<int, std::string>> get_lista_produtos();

void insere_produto_na_lista(std::string codigo, int quantidade, std::vector <Produto *> produtos);

void imprime_lista_produtos(std::vector <Produto *> produtos);

bool verifica_compra(std::vector <Produto *> produtos);

void atualiza_estoque(std::vector <Produto *> produtos);

void calcula_valor_total(bool socio);

double calcula_troco(double valor_pago);

# Produto:
A classe Produto é utilizada no Modo Estoque para se obter os dados do produto que o usuário deseja cadastrar no estoque.

## Atributos:
std::string nome;

std::string codigo;

int quantidade;

std::string categoria;

double valor;

## Funções:
Produto();

Produto(std::string nome, std::string codigo, int quantidade, std::string categoria, double valor);

~Produto();

void set_nome(std::string nome);

std::string get_nome();

void set_codigo(std::string codigo);

std::string get_codigo();

void set_quantidade(int quantidade);

int get_quantidade();

void set_categoria(std::string categoria);

std::string get_categoria();

void set_valor(double valor);

double get_valor();

bool produto_existe(std::vector <Produto *> produtos, std::string codigo);

std::vector <Produto *> adiciona_produto(std::vector <Produto *> produtos, int quantidade, std::string codigo);

std::vector <Produto *> cadastro(std::vector <Produto *> produtos, std::string nome, std::string categoria, double valor, std::string codigo, int quantidade);

void lista_estoque(std::vector <Produto *> produtos);
        
# Recomendacao:
A classe Recomendação é utilizada no Modo Recomendação para se obter as categorias mais
compradas pelo cliente buscado pelo usuário. Ela relaciona o cpf do cliente com as categorias
e quantidades que o cliente comprou anteriormente.

## Atributos:
std::string cpf;

std::vector <std::string> categorias;

std::vector <int> quantidades;

## Funções:
Recomendacao();

Recomendacao(std::string cpf);

~Recomendacao();

std::string get_cpf();

void set_cpf(std::string cpf);

std::vector <int> get_quantidades();

void set_quantidades(int quantidade);

std::vector <std::string> get_categorias();

void set_categoria(std::string categoria);

std::vector <Recomendacao *> atualiza_cliente_recomendacoes(std::string cpf,
                        std::vector <Recomendacao *> recomendacoes,
                        std::map <std::string, std::pair<int, std::string>> lista_produtos);

void lista_recomendacao(std::vector <std::string> categorias, std::vector <int> quantidades);
    
## Observação:
Nas pastas inc e src, existe um arquivo functions.hpp e functions.cpp, respectivamente.
Neles estão funções usadas em todo o programa que não se relacionam a nenhuma classe.
### Funções:
bool cliente_existe(std::string cpf, std::vector <Cliente *> clientes);

std::vector <std::string> separa_string(std::string str, std::vector <std::string> v);

bool valida_entrada(std::string entrada, char parametro1, char parametro2);

bool valida_cpf(std::string cpf);

bool valida_email(std::string email);

std::vector <Cliente *> lista_clientes(std::vector <Cliente *> clientes);

std::vector <Produto *> lista_produtos(std::vector <Produto *> produtos);

std::vector <Recomendacao *> lista_recomendacoes(std::vector <Recomendacao *> recomendacoes);

void atualiza_clientes(std::vector <Cliente *> clientes);

void atualiza_produtos(std::vector <Produto *> produtos);

void atualiza_recomendacoes(std::vector <Recomendacao *> recomendacoes);
